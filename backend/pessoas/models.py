from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _


GENERO = [('M','Masculino'), ('F','Feminino'), ('O','Outro')]


class Pessoa(models.Model):
	nome = models.CharField(max_length=120, null=False, blank=False, verbose_name=_(u"Nome"))
	data_nascimento = models.DateField(max_length=200, null=False, blank=False, verbose_name=_(u"Data de Nascimento"))
	sexo = models.CharField(max_length=200, null=False, blank=False, choices=GENERO, verbose_name=_(u"Gênero"))
	documento_identificacao = models.CharField(max_length=120, null=False, blank=False, verbose_name=_(u"Documento de identificação"))
	endereco = models.CharField(max_length=200, null=True, blank=True, verbose_name=_(u"Endereço"))

	class Meta:
		verbose_name = _(u'Pessoa')
		verbose_name_plural = _(u'Pessoas')




#         Objeto "Pessoa" terá os seguintes atributos: nome, data nascimento, documento de identificação, sexo, endereco.
#        Incluir validação de obrigatório para todos os campos: documento de identificação, nome, data nascimento e sexo.