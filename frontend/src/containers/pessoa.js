import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './pessoa.css'


class PostPessoa extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pessoas: []
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.getPessoas = this.getPessoas.bind(this);
    this.getPessoas();

  }

  handleSubmit(event) {
    event.preventDefault();
    const data = new FormData(event.target);
    
    
    this.setState({
      res: stringifyFormData(data),
    });
    fetch('http://localhost:8000/pessoas/', {
      method: 'POST',
      body: data,
    });
    
  }

  getPessoas(event) {

    const pessoasjson = fetch('http://localhost:8000/pessoas/?format=json', {
        method: 'GET',
        datatype:'json'
      })

    pessoasjson.then(response => response.text()) 
       .then(json => {                    
            
        this.setState({pessoas: json})


       })
       .catch(error => {

       });
 
 }



  componentDidUpdate(){
    this.getPessoas();
  }




  render() {
    const data = JSON.parse(JSON.stringify(this.state.pessoas));

    return (
      <div class="formdiv">
        <form onSubmit={this.handleSubmit}>
          <label htmlFor="nome">Nome</label>
          <input id="nome" name="nome" type="text" />

          <label htmlFor="data_nascimento">Data de nascimento (ano-mes-dia)</label>
          <input id="data_nascimento" name="data_nascimento" type="text" />

          <label htmlFor="documento_identificacao">Documento de identificação (RG) </label>
          <input id="documento_identificacao" name="documento_identificacao" type="text" />

          <label htmlFor="endereco">Endereço</label>
          <input id="endereco" name="endereco" type="text" />

          <label htmlFor="sexo">Gênero (M/F)</label>
          <input id="sexo" name="sexo" type="text" />


          <button>Cadastrar</button>
        </form>
        
        {this.state.res && (
          <div className="res-block">
            <h3>Seu formulário foi enviado à API:</h3>
            <pre>FormData {this.state.res}</pre>
          </div>
        )}

        <table className="table table-hover">
          <thead>
            <th>Pessoas</th>
          </thead>
          <tbody> 
            <td>{data}</td>
          </tbody>
        </table>

      </div>



    );
  }
}

function stringifyFormData(fd) {
  const data = {};
  for (let key of fd.keys()) {
    data[key] = fd.get(key);
  }
  return JSON.stringify(data, null, 2);
}

export default PostPessoa;