import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App';
import PostPessoa from './containers/pessoa'
import './containers/index.css';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);

ReactDOM.render(<PostPessoa />, document.getElementById('PostPessoa'));
